# Welcome
Collected here are several algorithms for sorting the visibility of their speed.

## In the presence of:
* bubble - [Bubble sort](http://en.wikipedia.org/wiki/Bubble_sort)
* minElement - [Selection sort](http://en.wikipedia.org/wiki/Selection_sort)
* inserts - [Insertion sort](http://en.wikipedia.org/wiki/Insertion_sort)
* shell - [Shell sort](http://en.wikipedia.org/wiki/Shellsort)
* heap - [Heap sort](http://en.wikipedia.org/wiki/Heapsort)
* quick - [Quick sort](http://en.wikipedia.org/wiki/Quicksort)
* sort - Default js array sort [].sort()

## How to use
1. Open  index.html in your browser
2. Open debug console 
3. Run:
```
#!javascript
Sort.test()

```

### Sort.test(int arrLength, int numberLength, int numberTimes)
* **arrLength** - array length - default 1000000
* **numberLength** - number length - default 7
* **numberTimes** - number times - default 3

### Sort.view(obj params)
Default displaing in console.log().  
Params gets from Sort.test and has: **arr**, **name**, **time** - middle, **times** - array with time iteration.

### Object Sort(int arrLength, int numberLength)
* **arr** - internal array
* **bubble()**
* **minElement()**
* **inserts()**
* **shell()**
* **heap()**
* **quick()**
* **sort()**

All methods returned **params**